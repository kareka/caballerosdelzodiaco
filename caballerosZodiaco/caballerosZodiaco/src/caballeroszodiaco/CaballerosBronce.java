/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caballeroszodiaco;

/**
 *
 * @author Jose Maria Osuna
 */
public class CaballerosBronce extends Caballero{
    private String constelacion;

    public String getConstelacion() {
        return constelacion;
    }

    public void setConstelacion(String constelacion) {
        this.constelacion = constelacion;
    }
   

    public CaballerosBronce(String constelacion, String nombre, String armadura, String procedencia, int vida, int cosmos, Ataque[] ataque) {
        super(nombre, armadura, procedencia, vida, cosmos, ataque);
        this.constelacion = constelacion;
    }

  
    
}
