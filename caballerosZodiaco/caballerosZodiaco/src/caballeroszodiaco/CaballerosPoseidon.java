/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caballeroszodiaco;

/**
 *
 * @author Jose Maria Osuna
 */
public class CaballerosPoseidon extends Caballero{
  private String monstruoMarino;     
   

    public CaballerosPoseidon(String monstruoMarino,   String nombre, String armadura, String procedencia,int vida, int cosmos, Ataque[] ataque) {
        super( nombre, armadura, procedencia,vida,cosmos, ataque);
        this.monstruoMarino = monstruoMarino;
        
    }

    public String getMonstruoMarino() {
        return monstruoMarino;
    }

    public void setMonstruoMarino(String monstruoMarino) {
        this.monstruoMarino = monstruoMarino;
    }

  
      
}
