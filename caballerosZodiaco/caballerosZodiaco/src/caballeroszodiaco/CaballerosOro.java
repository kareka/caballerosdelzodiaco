/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caballeroszodiaco;

/**
 *
 * @author Jose Maria Osuna
 */
public class CaballerosOro extends Caballero {
    private String casa;

    public CaballerosOro(String casa, String nombre, String armadura, String procedencia, int vida, int cosmos, Ataque[] ataque) {
        super(nombre, armadura, procedencia, vida, cosmos, ataque);
        this.casa = casa;
    }

    public String getCasa() {
        return casa;
    }

    public void setCasa(String casa) {
        this.casa = casa;
    }
     

  
        
}
