/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caballeroszodiaco;

import java.util.Scanner;

/**
 *
 * @author Jose Maria Osuna
 */
public class Caballero {
   
    
    private String nombre;
    private String armadura;
    private String procedencia;    
    private int vida;
    private int cosmos;
    private Ataque[]ataque;

  

      public Caballero( String nombre, String armadura, String procedencia,int vida,int cosmos, Ataque[] ataque) {
        
        
        this.nombre=nombre;
        this.armadura=armadura;
        this.procedencia=procedencia;
        this.vida=vida;
        this.cosmos=cosmos;
        this.ataque=ataque;
    }
      
    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getArmadura() {
        return armadura;
    }

    public void setArmadura(String armadura) {
        this.armadura = armadura;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public int getCosmos() {
        return cosmos;
    }

    public void setCosmos(int cosmos) {
        this.cosmos = cosmos;
    }

    public Ataque[] getAtaque() {
        return ataque;
    }

    public void setAtaque(Ataque[] ataque) {
        this.ataque = ataque;
    }
    
   
//Esta función nos da el resultado de enfrentarnos a cada adversario de los distintos escenarios
//He incluido una opción de continuar la lucha si el rival nos gana, para que el juego no se acabe tan pronto.
//También nos va a proporcionar los puntos obtenidos en cada enfrentamiento.    
    public static int resultadoLucha(Caballero jugador1,Caballero jugador2){
        String continuar;
        int puntos=0;
       
        while ((jugador1.getVida()>0)&&(jugador2.getVida()>0)){
            //Procedemos a introducir el ataque de nuestro jugador 
    
                Scanner sr=new Scanner(System.in);
                System.out.println("Elige un tipo de ataque :\t\n1-Golpe\t\n2-Poder\t\n3-Final ");
                int tipoAtaque=sr.nextInt();                
                 ataque(jugador1,jugador2,tipoAtaque);
                 System.out.println("\nEl caballero "+" "+jugador2.getNombre()+" "+"tiene"+" "+jugador2.getVida()+" "+"de vida."+" "+
                       "\nEl caballero "+" "+jugador1.getNombre()+" "+"tiene"+" "+jugador1.getCosmos()+" "+"de cosmos."    );
                    System.out.println("_______________________________________________________________________");
                   
                 
             //Aqui obtendremos el ataque de nuestro adversario      
                   int tipoAtaque2 = (int) (Math.random()*3+1);                   
                 ataque(jugador2,jugador1,tipoAtaque2);
                 System.out.println("\nEl caballero "+" "+jugador1.getNombre()+" "+"tiene"+" "+jugador1.getVida()+" "+"de vida."+" "+
                         "\nEl caballero "+" "+jugador2.getNombre()+" "+"tiene"+" "+jugador2.getCosmos()+" "+"de cosmos.");  
                   System.out.println("_______________________________________________________________________");                                
                   if(jugador1.getVida()==0){
                       System.out.println("\nEl caballero "+" " +jugador2.getNombre()+" "+"ha ganado la pelea");  
                       Scanner sc=new Scanner(System.in);
                       System.out.println("Deseas contibuar S/N:");
                       continuar=sc.nextLine();
                       if(continuar.equalsIgnoreCase("s")){                      
                                                      jugador1.setVida(100);
                                                      jugador2.setVida(100);
                                                      jugador1.setCosmos(100);
                                                      jugador2.setCosmos(100);
                       }else{
                           System.out.println("Juego terminado");
                           System.exit(0);
                           
                       }
                  }
                if(jugador2.getVida()==0){
                     System.out.println("\nEl caballero "+" " +jugador1.getNombre()+" "+"ha ganado la pelea");                                  
                                                     
                                                      puntos=puntos+33;
                                             
                }
             
            
       }
        return puntos;
    }
//Esta función hace que se ejecute cada ataque tanto de nuestro jugador como del rival,
//en ambos caso he utilizado un numero random para controlar si el ataque ha tenido exito o no.    
    public static void ataque(Caballero jugador1,Caballero jugador2,int tipoAtaque){
       
            
            switch(tipoAtaque){
                case 1 :
                 if(jugador1.getVida()!=0){   
                    System.out.println("Ataca"+" "+jugador1.getNombre());
                    if(jugador1.getCosmos()>=20){
                    System.out.println(jugador1.getAtaque()[0].getNombre());
                    jugador1.setCosmos(jugador1.getCosmos()-20);
                       //Vamos a generar un numero aleatorio para saber si el ataque ha sido efectivo o no
                    int numero = (int) (Math.random()*2+1);
                     if(numero==1){ 
                         System.out.println("El ataque ha tenido exito");
                        
                         if(jugador2.getVida()>20){
                         jugador2.setVida(jugador2.getVida()-20);
                         }else{
                             jugador2.setVida(0);
                         }
                     }else{
                         System.out.println("El ataque ha fallado");
                      
                         jugador2.setVida(jugador2.getVida());
                     }
                    }else{
                        System.out.println("No tiene suficiente cosmos para realizar un ataque.");
                      
                        int recargaCosmos=100-jugador1.getCosmos();
                        jugador1.setCosmos(jugador1.getCosmos()+recargaCosmos);
                     }
                 
                 }  
                  
                    
                break;
                case 2:
                 if(jugador1.getVida()!=0){   
                    if(jugador1.getCosmos()>=30){
                    System.out.println(jugador1.getAtaque()[1].getNombre());
                        jugador1.setCosmos(jugador1.getCosmos()-30);
                                  int numero = (int) (Math.random()*2+1);
                     if(numero==1){ 
                         System.out.println("El ataque ha tenido exito");
                        
                          if(jugador2.getVida()>30){
                         jugador2.setVida(jugador2.getVida()-30);
                         }else{
                             jugador2.setVida(0);
                         }
                     }else{
                         System.out.println("El ataque ha fallado");
                          
                         jugador2.setVida(jugador2.getVida());
                     }
                    }else{
                        System.out.println("No tiene suficiente cosmos para realizar un ataque");
                       
                          int recargaCosmos=100-jugador1.getCosmos();
                         jugador1.setCosmos(jugador1.getCosmos()+recargaCosmos);
                    }
                 }
                       
                break;
                case 3:
                 if(jugador1.getVida()!=0){     
                    if(jugador1.getCosmos()>=50){
                       System.out.println(jugador1.getAtaque()[2].getNombre());
                    
                       jugador1.setCosmos(jugador1.getCosmos()-50);
                        int numero = (int) (Math.random()*2+1);
                     if(numero==1){ 
                         System.out.println("El ataque ha tenido exito");
                          
                      if(jugador2.getVida()>50){
                        jugador2.setVida(jugador2.getVida()-50);
                         }else{
                             jugador2.setVida(0);
                         }
                     }else{
                         System.out.println("El ataque ha fallado");
                          
                         jugador2.setVida(jugador2.getVida());
                     }
                    }else{
                        System.out.println("No tiene suficiente cosmos para realizar un ataque");
                       
                           int recargaCosmos=100-jugador1.getCosmos();
                          jugador1.setCosmos(jugador1.getCosmos()+recargaCosmos);
                    }
                 }    
                      
                break;    
                 default:
                    System.out.println("No es una opción válida. Vuelve a introducir");
               break;
                 
            
      }
   
            
         
        
    }
//Esta función nos da el rival al que nos enfrentamos
//Esta hecha por medio de un numero aleatorio que recorre el array de caballeros que
//tiene cada escenario.    
    public static  Caballero  eligeRival(Escenario escenario,Caballero jugador1){//arreglar el fallo con un do..while                
         int numero ;              
         Caballero rival=null;
         do{
           numero= (int) (Math.random()*(escenario.getCaballerosPropios().length-1)+0);  
         }while(!((escenario.getCaballerosPropios()[numero].getVida()>0)&&(escenario.getCaballerosPropios()[numero]!=jugador1)));
              
                    rival=escenario.getCaballerosPropios()[numero];                                   
            
                  
                            
               return rival;
              
    }
 //Función que devuelve los puntos conseguidos en cada fase
//Tenemos un contador para controlar el numero de veces que se lucha
//En el caso de este programa son tres adversarios por nivel
      public static int faseJuego(Escenario escenario,Caballero jugador1){
           int contador=0;
           int pTotales=escenario.getPuntosTotales();
                                
                                         do{
                                              System.err.println("Los puntos totales son: "+pTotales);  
                                          Caballero jugador2=Caballero.eligeRival(escenario, jugador1);
                                          int puntos=Caballero.resultadoLucha(jugador1, jugador2);
                                            pTotales=pTotales+puntos;                                                                                   
                                           jugador1.setVida(100);
                                           jugador1.setCosmos(100);
                                          contador++;      
                                         
                                         }while(contador<4);
                                         escenario.setPuntosTotales(pTotales);
                                         return pTotales;
                                       
      
           }
}
   

