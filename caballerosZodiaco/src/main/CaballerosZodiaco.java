               /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;


import clases.Ataque;
import clases.Caballero;
import clases.CaballerosBronce;
import clases.CaballerosHilda;
import clases.CaballerosOro;
import clases.CaballerosPoseidon;
import clases.Escenario;
import clases.Usuario;
import excepciones.EdadExcepcion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Jose Maria Osuna
 */
public class CaballerosZodiaco {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * Vamos a crear los ataques personalizados por caballero 
         */
         //Ataques de los caballeros de bronce
        //Ataques del caballero del Dragon
        Ataque ataqueDragon1 = new Ataque("Excalibur", "golpe", 20, 20);
        Ataque ataqueDragon2 = new Ataque("Vuelo del dragon", "poder", 30, 30);
        Ataque ataqueDragon3 = new Ataque("Elevacion del dragon", "final", 50, 50);
        //Ataques del caballero de Andromeda
        Ataque ataqueAndromeda1 = new Ataque("Nebulosa de andromeda", "golpe", 20, 20);
        Ataque ataqueAndromeda2 = new Ataque("Cadena nebular", "poder", 30, 30);
        Ataque ataqueAndromeda3 = new Ataque("Defensa giratoria", "final", 50, 50);
        //Ataques del caballero del Cisne
        Ataque ataqueCisne1 = new Ataque("Puño de hielo", "golpe", 20, 20);
        Ataque ataqueCisne2 = new Ataque("Rayo de la aurora", "poder", 30, 30);
        Ataque ataqueCisne3 = new Ataque("Polvo de diamante", "final", 50, 50);
        //Ataques del caballero del Fenix
        Ataque ataqueFenix1 = new Ataque("Puño fantasma", "golpe", 20, 20);
        Ataque ataqueFenix2 = new Ataque("Alas ardientes", "poder", 30, 30);
        Ataque ataqueFenix3 = new Ataque("Ilusion diabolica", "final", 50, 50);
        //Ataques del caballero del Pegaso
        Ataque ataquePegaso1 = new Ataque("Meteoro de pegaso", "golpe", 20, 20);
        Ataque ataquePegaso2 = new Ataque("Choque giratorio", "poder", 30, 30);
        Ataque ataquePegaso3 = new Ataque("Cometa de pegaso", "final", 50, 50);

        //Ataques de los caballeros de oro
        //Ataques del caballero de Leo
        Ataque ataqueLeo1 = new Ataque("Puño de leo", "golpe", 200, 200);
        Ataque ataqueLeo2 = new Ataque("Kens de energia", "poder", 300, 300);
        Ataque ataqueLeo3 = new Ataque("Energia dorada", "final", 500, 500);
        //Ataques del caballero de Libra
        Ataque ataqueLibra1 = new Ataque("Escudo destructor", "golpe", 200, 200);
        Ataque ataqueLibra2 = new Ataque("Ataque del tigre", "poder", 300, 300);
        Ataque ataqueLibra3 = new Ataque("100 dragones de Rozan", "final", 500, 500);
        //Ataques del caballero de Virgo
        Ataque ataqueVirgo1 = new Ataque("Esfera de luz", "golpe", 200, 200);
        Ataque ataqueVirgo2 = new Ataque("Destruccion de los sentidos", "poder", 300, 300);
        Ataque ataqueVirgo3 = new Ataque("Destitucion terrenal", "final", 500, 500);
        //Ataques del caballero de Geminis
        Ataque ataqueGeminis1 = new Ataque("Puño de luz", "golpe", 200, 200);
        Ataque ataqueGeminis2 = new Ataque("Cosmos fulminante", "poder", 300, 300);
        Ataque ataqueGeminis3 = new Ataque("Explosion de galaxias", "final", 500, 500);
        //Ataques del caballero de Piscis
        Ataque ataquePiscis1 = new Ataque("Rosas piraña", "golpe", 200, 200);
        Ataque ataquePiscis2 = new Ataque("Rosas diabolicas", "poder", 300, 300);
        Ataque ataquePiscis3 = new Ataque("Rosa sangrienta", "final", 500, 500);
        //Ataques del caballero de Sagitario
        Ataque ataqueSagitario1 = new Ataque("Trueno atomico", "golpe", 200, 200);
        Ataque ataqueSagitario2 = new Ataque("Flecha de oro", "poder", 300, 300);
        Ataque ataqueSagitario3 = new Ataque("Ruptura infinita", "final", 500, 500);
        //Ataques del caballero de Tauro
        Ataque ataqueTauro1 = new Ataque("Puño de hierro", "golpe", 200, 200);
        Ataque ataqueTauro2 = new Ataque("Embestida del toro", "poder", 300, 300);
        Ataque ataqueTauro3 = new Ataque("Gran cuerno", "final", 500, 500);
        //Ataques del caballero de Acuario
        Ataque ataqueAcuario1 = new Ataque("Anillo de hielo", "golpe", 200, 200);
        Ataque ataqueAcuario2 = new Ataque("Ataud de hielo", "poder", 300, 300);
        Ataque ataqueAcuario3 = new Ataque("Ejecucion de la aurora", "final", 500, 500);
        //Ataques del caballero de Capricornio
        Ataque ataqueCapricornio1 = new Ataque("Excalibur infinita", "golpe", 200, 200);
        Ataque ataqueCapricornio2 = new Ataque("Desprendimiento de rocas", "poder", 300, 300);
        Ataque ataqueCapricornio3 = new Ataque("Danza de espadas", "final", 500, 500);
        //Ataques del caballero de Cancer
        Ataque ataqueCancer1 = new Ataque("Ondas infernales", "golpe", 200, 200);
        Ataque ataqueCancer2 = new Ataque("Espiral de luz", "poder", 300, 300);
        Ataque ataqueCancer3 = new Ataque("Agujero negro", "final", 500, 500);
        //Ataques del caballero de Aries
        Ataque ataqueAries1 = new Ataque("Muro de cristal", "golpe", 200, 200);
        Ataque ataqueAries2 = new Ataque("Red de cristal", "poder", 300, 300);
        Ataque ataqueAries3 = new Ataque("Espiral", "final", 500, 500);
        //Ataques del caballero de Escorpio
        Ataque ataqueEscorpio1 = new Ataque("Aguja escarlata", "golpe", 200, 200);
        Ataque ataqueEscorpio2 = new Ataque("Aguja de fuego", "poder", 300, 300);
        Ataque ataqueEscorpio3 = new Ataque("Esfera de energia", "final", 500, 500);

        //Ataques de los caballeros de Hilda
        //Ataques del caballero de Delta
        Ataque ataqueDelta1 = new Ataque("Escudo de amatista", "golpe", 2000, 2000);
        Ataque ataqueDelta2 = new Ataque("Espada llameante", "poder", 3000, 3000);
        Ataque ataqueDelta3 = new Ataque("Ataud de amatista", "final", 5000, 5000);
        //Ataques del caballero de Zeta
        Ataque ataqueZeta1 = new Ataque("Garra de tigre vikingo", "golpe", 2000, 2000);
        Ataque ataqueZeta2 = new Ataque("Sombra del tigre", "poder", 3000, 3000);
        Ataque ataqueZeta3 = new Ataque("Impulso azul", "final", 5000, 5000);
        //Ataques del caballero de Epsilon
        Ataque ataqueEpsilon1 = new Ataque("Garra feroz", "golpe", 2000, 2000);
        Ataque ataqueEpsilon2 = new Ataque("Lobo de la estepa", "poder", 3000, 3000);
        Ataque ataqueEpsilon3 = new Ataque("Ataque de la jauria", "final", 5000, 5000);
        //Ataques del caballero de Beta
        Ataque ataqueBeta1 = new Ataque("Rayo de fuego", "golpe", 2000, 2000);
        Ataque ataqueBeta2 = new Ataque("Congelacion del universo", "poder", 3000, 3000);
        Ataque ataqueBeta3 = new Ataque("Gran presion ardiente", "final", 5000, 5000);
        //Ataques del caballero de Gamma
        Ataque ataqueGamma1 = new Ataque("Martillo de Mjolnir", "golpe", 2000, 2000);
        Ataque ataqueGamma2 = new Ataque("Heracles titanico", "poder", 3000, 3000);
        Ataque ataqueGamma3 = new Ataque("Puño de titan", "final", 5000, 5000);
        //Ataques del caballero de Alpha
        Ataque ataqueAlpha1 = new Ataque("Poder de la estrella Alpha", "golpe", 2000, 2000);
        Ataque ataqueAlpha2 = new Ataque("Espada de Odin", "poder", 3000, 3000);
        Ataque ataqueAlpha3 = new Ataque("Ventisca de dragon", "final", 5000, 5000);
        //Ataques del caballero de Eta
        Ataque ataqueEta1 = new Ataque("Requiem de cuerdas", "golpe", 2000, 2000);
        Ataque ataqueEta2 = new Ataque("Aro de luz", "poder", 3000, 3000);
        Ataque ataqueEta3 = new Ataque("Tempano", "final", 5000, 5000);

        //Ataques de los caballeros de Poseidon
        //Ataques del caballero de Scylla
        Ataque ataqueScylla1 = new Ataque("Presa del aguila", "golpe", 20000, 20000);
        Ataque ataqueScylla2 = new Ataque("Estrangulacion de la serpiente", "poder", 30000, 30000);
        Ataque ataqueScylla3 = new Ataque("Gran tornado", "final", 50000, 50000);
        //Ataques del caballero de Kraken
        Ataque ataqueKraken1 = new Ataque("Aurora boreal", "golpe", 20000, 20000);
        Ataque ataqueKraken2 = new Ataque("Polvo de diamantes extremo", "poder", 30000, 30000);
        Ataque ataqueKraken3 = new Ataque("Torbellino polar", "final", 50000, 50000);
        //Ataques del caballero de Limnades
        Ataque ataqueLimnades1 = new Ataque("Metamorfosis", "golpe", 20000, 20000);
        Ataque ataqueLimnades2 = new Ataque("Sacudida", "poder", 30000, 30000);
        Ataque ataqueLimnades3 = new Ataque("Salamandra satanica", "final", 50000, 50000);
        //Ataques del caballero de Sirena
        Ataque ataqueSirena1 = new Ataque("Flauta dulce", "golpe", 20000, 20000);
        Ataque ataqueSirena2 = new Ataque("Sinfonia mortal", "poder", 30000, 30000);
        Ataque ataqueSirena3 = new Ataque("Climax mortal", "final", 50000, 50000);
        //Ataques del caballero de Chrysaor
        Ataque ataqueChrysaor1 = new Ataque("Lanzada destelleante", "golpe", 20000, 20000);
        Ataque ataqueChrysaor2 = new Ataque("Levitacion", "poder", 30000, 30000);
        Ataque ataqueChrysaor3 = new Ataque("Barrera de chakra", "final", 50000, 50000);
        //Ataques del caballero de Caballo Marino
        Ataque ataqueCaballoMarino1 = new Ataque("Oleadas ascendentes", "golpe", 20000, 20000);
        Ataque ataqueCaballoMarino2 = new Ataque("Soplo divino", "poder", 30000, 30000);
        Ataque ataqueCaballoMarino3 = new Ataque("Barrera protectora", "final", 50000, 50000);
        //Ataques del caballero de Dragon de los Mares
        Ataque ataqueDragonDeLosMares1 = new Ataque("Triangulo dorado", "golpe", 20000, 20000);
        Ataque ataqueDragonDeLosMares2 = new Ataque("Otra dimension", "poder", 30000, 30000);
        Ataque ataqueDragonDeLosMares3 = new Ataque("Explosion galactica", "final", 50000, 50000);
        
       
        //Vamos a asignar los ataques a los caballeros de bronce
        Ataque[]ataqueDragon={ataqueDragon1,ataqueDragon2,ataqueDragon3};
        Ataque[]ataquePegaso={ataquePegaso1,ataquePegaso2,ataquePegaso3};
        Ataque[]ataqueCisne={ataqueCisne1,ataqueCisne2,ataqueCisne3};
        Ataque[]ataqueFenix={ataqueFenix1,ataqueFenix2,ataqueFenix3};
        Ataque[]ataqueAndromeda={ataqueAndromeda1,ataqueAndromeda2,ataqueAndromeda3};
        
        //Vamos a asignar los ataques a los caballeros de oro
        Ataque[]ataqueLeo={ataqueLeo1,ataqueLeo2,ataqueLeo3};
        Ataque[]ataqueLibra={ataqueLibra1,ataqueLibra2,ataqueLibra3};
        Ataque[]ataqueVirgo={ataqueVirgo1,ataqueVirgo2,ataqueVirgo3};
        Ataque[]ataqueGeminis={ataqueGeminis1,ataqueGeminis2,ataqueGeminis3};
        Ataque[]ataqueAcuario={ataqueAcuario1,ataqueAcuario2,ataqueAcuario3};
        Ataque[]ataqueTauro={ataqueTauro1,ataqueTauro2,ataqueTauro3};
        Ataque[]ataqueCancer={ataqueCancer1,ataqueCancer2,ataqueCancer3};
        Ataque[]ataquePiscis={ataquePiscis1,ataquePiscis2,ataquePiscis3};
        Ataque[]ataqueCapricornio={ataqueCapricornio1,ataqueCapricornio2,ataqueCapricornio3};
        Ataque[]ataqueSagitario={ataqueSagitario1,ataqueSagitario2,ataqueSagitario3};
        Ataque[]ataqueEscorpio={ataqueEscorpio1,ataqueSagitario2,ataqueSagitario3};
        Ataque[]ataqueAries={ataqueAries1,ataqueAries2,ataqueAries3};
        
        //Vamos a asignar los ataques a los caballeros de hilda
        Ataque[]ataqueDelta={ataqueDelta1,ataqueDelta2,ataqueDelta3};
        Ataque[]ataqueBeta={ataqueBeta1,ataqueBeta2,ataqueBeta3};
        Ataque[]ataqueZeta={ataqueZeta1,ataqueZeta2,ataqueZeta3};
        Ataque[]ataqueGamma={ataqueGamma1,ataqueGamma2,ataqueGamma3};
        Ataque[]ataqueEpsilon={ataqueEpsilon1,ataqueEpsilon2,ataqueEpsilon3};
        Ataque[]ataqueEta={ataqueEta1,ataqueEta2,ataqueEta3};
        Ataque[]ataqueAlpha={ataqueAlpha1,ataqueAlpha2,ataqueAlpha3};
        
        //Vamos a asignar los ataques a los caballeros de poseidon
        Ataque[]ataqueScylla={ataqueScylla1,ataqueScylla2,ataqueScylla3};
        Ataque[]ataqueSirena={ataqueSirena1,ataqueSirena2,ataqueSirena3};
        Ataque[]ataqueKraken={ataqueKraken1,ataqueKraken2,ataqueKraken3};
        Ataque[]ataqueDragonDeLosMares={ataqueDragonDeLosMares1,ataqueDragonDeLosMares2,ataqueDragonDeLosMares3};
        Ataque[]ataqueCaballoMarino={ataqueCaballoMarino1,ataqueCaballoMarino2,ataqueCaballoMarino3};
        Ataque[]ataqueLimnades={ataqueLimnades1,ataqueLimnades2,ataqueLimnades3};
        Ataque[]ataqueChrysaor={ataqueChrysaor1,ataqueChrysaor2,ataqueChrysaor3};
        
      

        //Vamos a crear los caballeros de Bronce
        CaballerosBronce caballeroBronceDragon = new CaballerosBronce("Dragon","Shiryu","bronce","China",100,100,ataqueDragon);
        CaballerosBronce caballeroBroncePegaso = new CaballerosBronce("Pegaso","Seiya","bronce","Grecia",100,100,ataquePegaso);
        CaballerosBronce caballeroBronceFenix = new CaballerosBronce("Fenix","Ikki","bronce","Ecuador",100,100,ataqueFenix);
        CaballerosBronce caballeroBronceAndromeda = new CaballerosBronce("Andromeda","Shun","bronce","Etiopia",100,100,ataqueAndromeda);
        CaballerosBronce caballeroBronceCisne = new CaballerosBronce("Cisne","Hyoga","bronce","Rusia",100,100,ataqueCisne);
       
        //Vamos a crear los caballeros de Oro
        CaballerosOro caballeroOroLeo = new CaballerosOro("Leo","Aiora","oro","Grecia",1000,1000,ataqueLeo);
        CaballerosOro caballeroOroLibra = new CaballerosOro("Libra","Dokho","oro","China",1000,1000,ataqueLibra);
        CaballerosOro caballeroOroVirgo = new CaballerosOro("Virgo","Shaka","oro","India",1000,1000,ataqueVirgo);
        CaballerosOro caballeroOroGeminis = new CaballerosOro("Geminis","Saga","oro","Grecia",1000,1000,ataqueGeminis);
        CaballerosOro caballeroOroPiscis = new CaballerosOro("Piscis","Afrodita","oro","Suecia",1000,1000,ataquePiscis);
        CaballerosOro caballeroOroSagitario = new CaballerosOro("Sagitario","Aioros","oro","Grecia",1000,1000,ataqueSagitario);
        CaballerosOro caballeroOroTauro = new CaballerosOro("Tauro","Aldebaran","oro","Brasil",1000,1000,ataqueTauro);
        CaballerosOro caballeroOroAcuario = new CaballerosOro("Acuario","Camus","oro","Francia",1000,1000,ataqueAcuario);
        CaballerosOro caballeroOroCapricornio = new CaballerosOro("Capricornio","Shura","oro","España",1000,1000,ataqueCapricornio);
        CaballerosOro caballeroOroCancer = new CaballerosOro("Cancer","Death Mask","oro","Italia",1000,1000,ataqueCancer);
        CaballerosOro caballeroOroAries = new CaballerosOro("Aries","Mu","oro","Tibet",1000,1000,ataqueAries);
        CaballerosOro caballeroOroEscorpio = new CaballerosOro("Escorpio","Milo","oro","Grecia",1000,1000,ataqueEscorpio);
        
        
        //Vamos a crear los caballeros de Hilda
         CaballerosHilda caballeroHildaDelta = new CaballerosHilda("Delta","Alberich","zafiro","Noruega",10000,10000,ataqueDelta);
         CaballerosHilda caballeroHildaZeta = new CaballerosHilda("Zeta","Syd","zafiro","Noruega",10000,10000,ataqueZeta);
         CaballerosHilda caballeroHildaEpsilon = new CaballerosHilda("Epsilon","Fenrir","zafiro","Noruega",10000,10000,ataqueEpsilon);
         CaballerosHilda caballeroHildaBeta = new CaballerosHilda("Beta","Hagen","zafiro","Noruega",10000,10000,ataqueBeta);
         CaballerosHilda caballeroHildaGamma = new CaballerosHilda("Gamma","Thor","zafiro","Noruega",10000,10000,ataqueGamma);
         CaballerosHilda caballeroHildaAlpha = new CaballerosHilda("Alpha","Sigfried","zafiro","Noruega",10000,10000,ataqueAlpha);
         CaballerosHilda caballeroHildaEta = new CaballerosHilda("Eta","Mime","zafiro","Noruega",10000,10000,ataqueEta);
          
          
          //Vamos a crear los caballeros de Poseidon
         CaballerosPoseidon caballeroPoseidonScylla = new CaballerosPoseidon("Scylla","Io","escamas","Chile",100000,100000,ataqueScylla);
         CaballerosPoseidon caballeroPoseidonKraken = new CaballerosPoseidon("Kraken","Isaac","escamas","Finlandia",100000,100000,ataqueKraken);
         CaballerosPoseidon caballeroPoseidonLimnades = new CaballerosPoseidon("Limnades","Kasa","escamas","Portugal",100000,100000,ataqueLimnades);
         CaballerosPoseidon caballeroPoseidonSirena = new CaballerosPoseidon("Sirena","Sorrento","escamas","Austria",100000,100000,ataqueSirena);
         CaballerosPoseidon caballeroPoseidonChrysaor = new CaballerosPoseidon("Chrysaor","Krishna","escamas","India",100000,100000,ataqueChrysaor);
         CaballerosPoseidon caballeroPoseidonCaballoMarino = new CaballerosPoseidon("Caballo Marino","Baian","escamas","Canada",100000,100000,ataqueCaballoMarino);
         CaballerosPoseidon caballeroPoseidonDragonDeLosMares = new CaballerosPoseidon("Dragon de los Mares","Kanon","escamas","Grecia",100000,100000,ataqueDragonDeLosMares);
        
          //Vamos a introducir los caballeros en un array 
          Caballero[]caballeroBronce={caballeroBronceDragon,caballeroBroncePegaso,caballeroBronceCisne,caballeroBronceAndromeda,caballeroBronceFenix};
          Caballero[]caballeroOro={caballeroOroLeo,caballeroOroLibra,caballeroOroSagitario,caballeroOroAcuario,caballeroOroCancer,caballeroOroVirgo,caballeroOroTauro,caballeroOroAries,caballeroOroCapricornio,caballeroOroEscorpio,caballeroOroGeminis,caballeroOroPiscis};
          Caballero[]caballeroHilda={caballeroHildaDelta,caballeroHildaZeta,caballeroHildaEpsilon,caballeroHildaBeta,caballeroHildaGamma,caballeroHildaAlpha,caballeroHildaEta};
          Caballero[]caballeroPoseidon={caballeroPoseidonScylla,caballeroPoseidonKraken,caballeroPoseidonLimnades,caballeroPoseidonSirena,caballeroPoseidonChrysaor,caballeroPoseidonCaballoMarino,caballeroPoseidonDragonDeLosMares};
            Escenario torneo = new Escenario("Torneo Galáctico",caballeroBronce,0);
            Escenario santuario = new Escenario("Santuario",caballeroOro,99);
            Escenario asgard = new Escenario("Asgard",caballeroHilda,198);
            Escenario poseidon = new Escenario("Templo Submarino",caballeroPoseidon,297);
          
        Scanner sc=new Scanner(System.in);
        Connection conexion=null;
        try {       
            conexion=DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/mydb1","root","");
            System.out.println("Estoy conectado");
        } catch (SQLException ex) {
             System.err.println("La conexión a base de datos a fallado");
             ex.printStackTrace();
        }
          
        //Vamos a realizar un menú con las opciones de juego
          int opcion = -1;
          int opcion2=-1;
          Usuario jugador=null;
        String menu="\nElige una opción (Introduce el número): "+"\n\t1-Introducir jugador nuevo."+"\n\t2-Continuar con un jugador registrado"+"\n\t0-Salir.";
        
         while (opcion != 0) {
            opcion=introduceInt(menu);
            switch (opcion) {
                case 1:
                    
                     jugador=registrarUsuario(sc,conexion);
                    
                        
                     String menu2="\nElige una opción (Introduce el número): "+"\n\t1- Modo historia"+"\n\t2-Modo arcade."+"\n\t0-Salir.";
                     while (opcion2 != 0) {
                        opcion2=introduceInt(menu2);
                         switch (opcion2) {
                            case 1:
                                 
                                 System.out.println("Elige un caballero:\n\t1-Pegaso\n\t2-Dragon\n\t3-Cisne\n\t4-Andromeda\n\t5-Fenix) ");
                                 String bronce=sc.nextLine();
                                 switch(bronce){
                                     case "1":
                                         System.out.println(caballeroBroncePegaso.getNombre()+" "+"es el caballero de"+" "+caballeroBroncePegaso.getConstelacion()+" "+",es de origen asiático, pero fué entrenado en"+" "+caballeroBroncePegaso.getProcedencia()
                                         +" "+",su maestra fue Marin (una caballero de plata que vivia en la acrópolis). Fue victima de burlas y constantes palizas por su origen,\npero derroto a Cassius y pudo conseguir la armadura de Pegaso.\nSu principal motivación es"
                                                 + "la de recuperar a su hermana Seika de la que fué separado en su infancia.");
                                        
                                        
                                         Caballero jugador1=caballeroBroncePegaso;
                                         int puntos=Caballero.faseJuego(torneo, jugador1);
                                     break;
                                     case "2":
                                          System.out.println(caballeroBronceDragon.getNombre()+" "+"es el caballero de"+" "+caballeroBronceDragon.getConstelacion()+" "+"fué entrenado en"+" "+caballeroBronceDragon.getProcedencia()
                                         +" "+",su maestro fue Dohko (una caballero de oro, aunque mantenia su identidad en secreto bajo el aspecto de un pequeño anciano). \nSu entrenamiento fue a través del conocimiento de su cuerpo y la naturaleza,\nTiene un tatuaje en la espalda que sólo aparece cuando su cosmos está al maximo. La garra del Dragón de su tatuaje muestra el lugar exacto de su corazón lo que le hace vulnerable.\nSu principal motivación es la busqueda de los causantes de la muerte de su amada Shunrei.");
                                         System.out.println("\n"+"El rival en el"+" "+torneo.getNombre()+" "+"es"+" "+caballeroBronceAndromeda.getNombre()+" "+"el caballero de"+" "+caballeroBronceAndromeda.getConstelacion());                                         
                                         jugador1=caballeroBronceDragon;
                                      break;
                                     case "3":
                                         System.out.println(caballeroBronceCisne.getNombre()+" "+"es el caballero de"+" "+caballeroBronceCisne.getConstelacion()+" "+"fué entrenado en"+" "+caballeroBronceCisne.getProcedencia()
                                                 +" "+",su maestro fué Cystal Saint (a su vez discipulo de Camus de Acuario).\nSu motivación es la de conseguir el poder necesario para rescatar a su madre muerta que yace en el fondo del océano Ártico dentro de un barco");
                                          System.out.println("\n"+"El rival en el"+" "+torneo.getNombre()+" "+"es"+" "+caballeroBroncePegaso.getNombre()+" "+"el caballero de"+" "+caballeroBroncePegaso.getConstelacion());                                         
                                          jugador1=caballeroBronceCisne;
                                      break;
                                     case "4":
                                         System.out.println(caballeroBronceAndromeda.getNombre()+" "+"es el caballero de "+" "+caballeroBronceAndromeda.getConstelacion()+" "+"fué entrenado en"+" "+caballeroBronceAndromeda.getProcedencia()
                                         +" "+",su maestro fue Daidalos de Cefeo. \nFue entrenado en el uso de las cadenas de su constelación como armas, asi como en el cuerpo a cuerpo.\nSu principal motivación es la de volver a reunirse con su hermano Ikki, del cual lo separaron a muy pronta edad.");
                                         System.out.println("\n"+"El rival en el"+" "+torneo.getNombre()+" "+"es"+" "+caballeroBronceDragon.getNombre()+" "+"el caballero de"+" "+caballeroBronceDragon.getConstelacion());
                                         
                                         jugador1=caballeroBronceAndromeda;
                                     break;
                                     case "5":
                                         System.out.println(caballeroBronceFenix.getNombre()+" "+"es el caballero de "+" "+caballeroBronceFenix.getConstelacion()+" "+"fué entrenado en"+" "+caballeroBronceFenix.getProcedencia()
                                         +" "+",su maestro fue Guilty \nSu entrenamiento fue a través de la dureza y la inculcación del odio.\nSu principal motivación es la de convertirse en el caballero más poderoso y así vengarse de los que lo trataron mal en su infancia.");
                                         System.out.println("\n"+"1El rival en el"+" "+torneo.getNombre()+" "+"es"+" "+caballeroBronceCisne.getNombre()+" "+"el caballero de"+" "+caballeroBronceCisne.getConstelacion());         
                                         jugador1=caballeroBronceFenix;                                                                                
                                     break;
                                     default:
                                        System.out.println("No es una opción válida. Vuelve a introducir");
                                    break;                                 
                                 } 
                                
                             break;
                        
                      
             
                         case 2:
                               
                                System.out.println("Elige un equipo: ");
                               String armadura=sc.nextLine();
                                   switch(armadura){
                                            case "Atenea":
                                            break;
                                            case "Patriarca":
                                            break;
                                            case "Odin":
                                            break;
                                            case "Poseidon":
                                            break;
                                    }    
                           break;
                        case 0:
                           System.out.println("Saliendo del programa");
                        break;
            
                        default:
                            System.out.println("No es una opción válida. Vuelve a introducir");
                         break;
                        }
                  break;
                     
                     }
                    
                case 2:
                   
                break;  
            }
           }
          } 
          


    
    
       public static String introduceString(String msg){
            System.out.println(msg);
            Scanner sc=new Scanner(System.in);
            String ret=sc.nextLine();
            if(ret.equals("salir")){
                System.exit(0);
            }
            return ret;
    }
    
    public static int introduceInt(String msg){
        return Integer.parseInt(introduceString(msg));
    }
    
    public static Usuario registrarUsuario(Scanner sc, Connection conn){
        Usuario actual=null;
        try {
            System.out.println("Introduce el nombre: ");
            String nombre=sc.nextLine();
            System.out.println("Introduce los apellidos: ");
            String apellidos=sc.nextLine();         
            System.out.println("Introduce contraseña: ");
            String contrasena=sc.nextLine();
            System.out.println("Introduce edad: ");
            int edad=sc.nextInt();
             actual= new Usuario(nombre,apellidos,edad,contrasena,null);
             Statement registerStatement=conn.createStatement();         
             registerStatement.executeUpdate("insert into mydb1.usuario(nombre,apellidos,edad,contrasena,caballero_nombre)"
                + " values('"+nombre+"','"+apellidos+"','"+edad+"','"+contrasena+"','null')");
        
        } catch (EdadExcepcion ex) {
            System.err.println("Error al introducir edad");
            ex.printStackTrace();
        } catch (SQLException ex) {
            System.err.println("Error al consultar la base de datos");
           ex.printStackTrace();
        }
        
        return actual;
    }
    public static void comprobarRegistro(Scanner sc, Connection conn){
        try {
            System.out.println("¿Como te llamas?");
            String nombre=sc.nextLine();
            System.out.println("Introduce la contraseña");
            String contrasena=sc.nextLine();
            String[] queryValues={nombre,contrasena};
            PreparedStatement loginStatement=conn.prepareStatement(
                    "select * from user where nombre=? and contrasena=? ");
            loginStatement.setString(1,nombre);
            loginStatement.setString(2,contrasena);
            ResultSet foundUser=loginStatement.executeQuery();
            if(foundUser.next()){
                System.out.println("Se ha registrado correctamente");
            }else{
               System.err.println("No se ha podido registrar");               
           }
        } catch (SQLException ex) {
            System.err.println("Excepción SQL");
        }
    }
            
}
             
    

