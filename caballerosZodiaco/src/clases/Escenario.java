/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.util.Scanner;

/**
 *
 * @author Jose Maria Osuna
 */
public class Escenario {
    //Nombre del escenario de lucha
    private String nombre;
    //Caballeros pertenecientes a cada escenario
    private Caballero[]caballerosPropios;
    private int puntosTotales;
   

    public Escenario(String nombre, Caballero[] caballerosPropios,int puntosTotales) {
        this.nombre = nombre;
        this.caballerosPropios = caballerosPropios;
        this.puntosTotales=puntosTotales;
    }

    public int getPuntosTotales() {
        return puntosTotales;
    }

    public void setPuntosTotales(int puntosTotales) {
        this.puntosTotales = puntosTotales;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Caballero[] getCaballerosPropios() {
        return caballerosPropios;
    }

    public void setCaballerosPropios(Caballero[] caballerosPropios) {
        this.caballerosPropios = caballerosPropios;
    }

    
}
