/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Jose Maria Osuna
 */
public class Ataque {
    //Nombre del ataque
    private String nombre;
    //Tipo de ataque, puede ser: "golpe","poder","final"
    private String tipo;    
    //Daño que provoca el ataque en el adversario
    private int nivelDano; 
    //Nivel de energia que sirve para realizar ataques
    private int gastoCosmos;

    public Ataque(String nombre, String tipo, int nivelDano, int gastoCosmos) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.nivelDano = nivelDano;
        this.gastoCosmos = gastoCosmos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getNivelDano() {
        return nivelDano;
    }

    public void setNivelDano(int nivelDano) {
        this.nivelDano = nivelDano;
    }

    public int getGastoCosmos() {
        return gastoCosmos;
    }

    public void setGastoCosmos(int gastoCosmos) {
        this.gastoCosmos = gastoCosmos;
    }
    
}
