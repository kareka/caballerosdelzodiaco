/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import excepciones.EdadExcepcion;

/**
 *
 * @author IreneJose
 */
public class Usuario {
    String nombre;
    String apellidos;
    int edad;
    String contrasena;
    Caballero caballero;
    int puntos;

    public Usuario(String nombre, String apellidos, int edad, String contrasena, Caballero caballero) throws EdadExcepcion {
        this.nombre = nombre;
        this.apellidos = apellidos;
         this.setEdad(edad); 
        this.contrasena = contrasena;
        this.caballero = caballero;
        this.puntos = 0;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) throws EdadExcepcion {
        if(edad>=16){
            this.edad = edad;
        }else{
            throw new EdadExcepcion("No tienes la edad permitida para poder jugar");
        }
        
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public Caballero getCaballero() {
        return caballero;
    }

    public void setCaballero(Caballero caballero) {
        this.caballero = caballero;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }
    
}
